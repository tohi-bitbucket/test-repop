FROM ubuntu:18.04
# Install dependencies
RUN apt-get update && apt-get -y install apache2=2.4.29-1ubuntu4.16
# Install apache and write hello world message
RUN echo 'Hello World!!!!!!DeployTest: 20210910' > /var/www/html/index.html
RUN echo 'Heeeeeeeeeelooooooooooooooo:20210910' >> /var/www/html/index.html
RUN echo '<br>' >> /var/www/html/index.html
RUN echo 'Hello World!!!!!!DeployTest: 20210910' >> /var/www/html/index.html
# Configure apache
RUN echo '#!/bin/bash' > /root/run_apache.sh
RUN echo '. /etc/apache2/envvars' >> /root/run_apache.sh && \
echo 'mkdir -p /var/run/apache2' >> /root/run_apache.sh && \
echo 'mkdir -p /var/lock/apache2' >> /root/run_apache.sh && \
echo '/usr/sbin/apache2 -D FOREGROUND' >> /root/run_apache.sh && \
echo 'localhost' > /etc/hostname && \
chmod 755 /root/run_apache.sh
EXPOSE 80
ENTRYPOINT ["/root/run_apache.sh"]
